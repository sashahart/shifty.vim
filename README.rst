shifty
######

This Vim plugin lets you use Shift-arrow keys to do visual selection without
first pressing v. This behavior is similar to what you'll see in a lot of
editors other than Vim.

Note that this suppresses some default vim keys, e.g. s-left for stepping left
by one word. I rarely used those keys, preferring motions like 'b'. But I don't
use the code in this plugin any more either. 
