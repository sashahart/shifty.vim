" In normal mode, start visual mode when shift-arrows are used.
nnoremap <s-left> v<left>
nnoremap <s-right> v<right>
nnoremap <s-up> v<up>
nnoremap <s-down> v<down>

" If we're already in visual mode, ignore the shift and just move.
vnoremap <s-left> <left>
vnoremap <s-right> <right>
vnoremap <s-up> <up>
vnoremap <s-down> <down>

" In insert mode, go into select mode when shift-arrows are used.
inoremap <s-left> <ESC>gh<left>
inoremap <s-right> <ESC>gh<right>
inoremap <s-down> <ESC>gh<down>
inoremap <s-up> <ESC>gh<up>

" If we're already in select mode, ignore the shift and just move.
snoremap <s-left> <left>
snoremap <s-right> <right>
snoremap <s-up> <up>
snoremap <s-down> <down>
